<?php

namespace GeoPagos;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    public $primaryKey  = 'codigousuario';

    protected $hidden = ['clave'];

    public function favoritos()
    {
        return $this->hasMany('GeoPagos\Favoritos', 'codigousuario');
    }

    public function pagos() {
       return $this->belongsToMany('GeoPagos\Pagos', 'usuariospagos', 'codigousuario', 'codigopago');
    }
}