<?php

namespace GeoPagos;

use Illuminate\Database\Eloquent\Model;

class Favoritos extends Model
{
    public function usuarios()
    {
        return $this->belongsTo('Usuarios', 'codigousuario');
    }
}
