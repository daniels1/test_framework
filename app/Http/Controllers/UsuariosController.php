<?php

namespace GeoPagos\Http\Controllers;

use Illuminate\Http\Request;
use GeoPagos\Usuarios;
use DB;

class UsuariosController extends Controller
{
    public function index(){
        
        $users = Usuarios::all();

        $usuarios = $users->toArray();

        foreach ($usuarios as $user) {

            $user['favoritos'] = DB::select('select u.codigousuario, u.usuario, u.avatar from usuarios u, favoritos f where u.codigousuario = f.codigousuariofavorito and f.codigousuario = ?', [$user['codigousuario']]);
            $arr_user[] = $user;
                        
        }

        return view('usuarios', ['users' => $arr_user]);

    }

    public function show($id){
        
        $user = Usuarios::find($id);
        $users = Usuarios::all();

        $favoritos = DB::select('select u.codigousuario, u.usuario, u.avatar from usuarios u, favoritos f where u.codigousuario = f.codigousuariofavorito and f.codigousuario = ?', [$user->codigousuario]);

        $data = [
            'user'      => $user,
            'users'     => $users,
            'favoritos' => count($favoritos) > 0 ? $favoritos : [],
        ];

        return view('detalle_usuarios', $data);

    }
}
