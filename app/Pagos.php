<?php

namespace GeoPagos;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{
    public $primaryKey  = 'codigopago';

    public function usuarios()
    {
        return $this->belongsToMany('GeoPagos\Usuarios', 'usuariospagos', 'codigopago', 'codigousuario');
    }
}
