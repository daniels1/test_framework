<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariospagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('usuariospagos', function (Blueprint $table) {
            $table->integer('codigopago');
            $table->foreign('codigopago')->references('codigopago')->on('pagos');
            $table->integer('codigousuario');
            $table->foreign('codigousuario')->references('codigousuario')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuariospagos');
    }
}
