<?php

use GeoPagos\Pagos;
use Illuminate\Database\Seeder;

class PagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pagos::create([
            'codigopago' => 1,
            'importe' => rand(1, 10000000),
            'fecha' => '2018-03-13'
        ]);

        Pagos::create([
            'codigopago' => 2,
            'importe' => rand(1, 10000000),
            'fecha' => '2018-03-10'
        ]);

        Pagos::create([
            'codigopago' => 3,
            'importe' => rand(1, 10000000),
            'fecha' => '2018-03-11'
        ]);

        Pagos::create([
            'codigopago' => 4,
            'importe' => rand(1, 10000000),
            'fecha' => '2018-03-11'
        ]);

        Pagos::create([
            'codigopago' => 5,
            'importe' => rand(1, 10000000),
            'fecha' => '2018-03-11'
        ]);
    }
}
