<?php

use Illuminate\Database\Seeder;
use \GeoPagos\Favoritos;

class FavoritosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Favoritos::create([
            'codigousuario' => 1,
            'codigousuariofavorito' => 3
        ]);
        
        Favoritos::create([
            'codigousuario' => 1,
            'codigousuariofavorito' => 4
        ]);

        Favoritos::create([
            'codigousuario' => 1,
            'codigousuariofavorito' => 7
        ]);
    }
}
