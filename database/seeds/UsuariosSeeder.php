<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use GeoPagos\Usuarios;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'usuarios',
        ]);

        Usuarios::create([
                'codigousuario' => 1,
                'usuario' => 'Daniel Sánchez',
                'avatar' => 'https://bootdey.com/img/Content/avatar/avatar7.png',
                'clave' => bcrypt('12345'),
                'edad' => '32'
        ]);
        
        factory(Usuarios::class, 7)
            ->create()
            ->each(function ($u) {
                $u->pagos()->save(factory(\GeoPagos\Pagos::class)->make());
            });
    }

    public function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
 
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
 
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }

}
