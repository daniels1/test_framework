<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GeoPagos\Usuarios::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'usuario' => $faker->name,
        'avatar' => 'https://bootdey.com/img/Content/avatar/avatar'.$faker->numberBetween(1, 6).'.png',
        'clave' => $password ?: $password = bcrypt('secret'),
        'edad' => $faker->numberBetween($min = 10, $max = 99)
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GeoPagos\Pagos::class, function (Faker\Generator $faker) {
    return [
        'importe' => $faker->numberBetween($min = 1, $max = 99999999),
        'fecha' => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GeoPagos\UsuariosPagos::class, function (Faker\Generator $faker) {
    return [
        'codigopago' => $faker->numberBetween($min = 10, $max = 99),
        'codigousuario' => $faker->numberBetween($min = 10, $max = 99)
    ];
});
