@extends('layout')

@section('content')

  @forelse($users as $user)
   <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card agent">
            <div class="agent-avatar"> 
                <a href="usuarios/{{$user['codigousuario']}}">
                  <img src="{{$user['avatar']}}" alt="user" class="img-fluid " >
                </a>
            </div>
            <div class="agent-content">
                <div class="agent-name">
                    <h4><a href="usuarios/{{$user['codigousuario']}}" class="profile-link">{{$user['usuario']}}</a></h4>
                    Edad: {{$user['edad']}} años
                    <ul class="list-unstyled team-info m-b-0">
                        <li class="m-r-15"><small>Favoritos</small></li>
                        @forelse($user['favoritos'] as $favorito)
                            <li>
                                <a href="{{ url('usuarios/'.$favorito->codigousuario) }}">
                                    <img src="{{$favorito->avatar}}" title="{{$favorito->usuario}}" alt="{{$favorito->usuario}}">
                                </a>
                            </li>
                        @empty
                            <p></p>
                        @endforelse
                    </ul>
                </div>
                
            </div>
        </div>
    </div>

  @empty
    <h3>No hay usuarios registrados.</h3>
  @endforelse

@endsection  