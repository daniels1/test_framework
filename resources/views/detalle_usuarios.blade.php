@extends('layout')

@section('content')
<div class="container profile-page">
    <div class="row">

        <div class="col-lg-3 col-sm-12">
            <div class="card agent">
                <div class="agent-avatar"> 
                    <a href="{{ url('usuarios/'.$user->codigousuario) }}">
                        <img src="{{$user->avatar}}" class="img-fluid " title="{{$user->usuario}}" alt="{{$user->usuario}}">
                    </a> 
                </div>
                <div class="agent-content">
                    <div class="agent-name">
                        <h4><a href="{{ url('usuarios/'.$user->codigousuario) }}">{{$user->usuario}}</a></h4>
                        <span>{{$user->edad}} años</span>
                        <ul class="list-unstyled team-info m-b-0">
                            <li class="m-r-15"><small>Favoritos</small></li>
                            @forelse($favoritos as $favorito)
                                <li>
                                    <a href="{{ url('usuarios/'.$favorito->codigousuario) }}">
                                        <img src="{{$favorito->avatar}}" title="{{$favorito->usuario}}" alt="{{$favorito->usuario}}">
                                    </a>
                                </li>
                            @empty
                                <p></p>
                            @endforelse

                        </ul>
                        <a href="{{url('/')}}" class="btn btn-success">Home <i class="fa fa-home"></i></a>
                        <a href="{{url()->previous()}}" class="btn btn-primary">Volver</a>
                    </div>                
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div class="main-box clearfix">
                <div class="table-responsive">
                    <table class="table user-list">
                        <thead>
                            <tr>
                                <th><span>Usuarios Registrados</span></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($users as $u)
                            @if($u->codigousuario != $user->codigousuario)
                                <tr>
                                    <td>
                                        <a href="{{ url('usuarios/'.$u->codigousuario) }}">
                                            <img src="{{$u->avatar}}" class="img-fluid " title="{{$u->usuario}}" alt="{{$u->usuario}}" width="64" height="64">
                                        </a> 
                                        <a href="{{ url('usuarios/'.$u->codigousuario) }}" class="user-link">{{$u->usuario}}</a>
                                        <span class="user-subhead">{{$u->edad}} años</span>
                                    </td>
                                    <td>
                                        boton Favorito
                                    </td>
                                </tr>
                            @endif
                            @empty
                                <tr><td colspan="2"></td></tr>
                            @endforelse                           
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>

        
	</div>
</div>
@endsection  