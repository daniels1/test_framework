
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--  This file has been downloaded from bootdey.com    @bootdey on twitter -->
    <!--  All snippets are MIT license http://bootdey.com/license -->
    <!-- 
    	The codes are free, but we require linking to our web site.
    	Why to Link?
    	A true story: one girl didn't set a link and had no decent date for two years, and another guy set a link and got a top ranking in Google! 
    	Where to Put the Link?
    	home, about, credits... or in a good page that you want
    	THANK YOU MY FRIEND!
    -->
    <title>Usuarios - GeoPagos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery.1.10.2.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/geopagos.css" rel="stylesheet">
    <style type="text/css">
    	body{
    margin-top:20px;
    background:#FAFAFA;    
}


    </style>
</head>
<body>
<div class="container">
  
    <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="people-nearby">
              @forelse($users as $user)
              <div class="nearby-user">
                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <a href="usuarios/{{$user->codigousuario}}"><img src="{{$user->avatar}}" alt="user" class="profile-photo-lg"></a>
                  </div>
                  <div class="col-md-7 col-sm-7">
                    <h5><a href="usuarios/{{$user->codigousuario}}" class="profile-link">{{$user->usuario}}</a></h5>
                    Edad: {{$user->edad}} años
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <a class="btn btn-primary btn-sm pull-right">Favoritos <i class="fas fa-star"></i></a>
                    <a class="btn btn-success btn-sm pull-right">Pagos <i class="fas fa-money-bill-alt"></i></a>
                  </div>
                </div>
              </div>
              @empty
                  <li>No hay usuarios registrados.</li>
              @endforelse
            </div>
    	</div>
	</div>
</div>

<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
	
</script>
</body>
</html>